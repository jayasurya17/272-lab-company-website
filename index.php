<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Elementary</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php 
            require 'navbar.php';
        ?>
        <div class="container mt-5">

            <div class="row">
                <div class="col-md-6">
                    <h3 class="display-3 font-weight-bold">Did you know?</h3>
                    <h3 class="display-4">Children learn better when learning is fun.</h3>
                </div>
                <div class="col-md-6">
                    <img src="/resources/img/image1.jpg" class="img-fluid">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <img src="/resources/img/image2.jpg" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <h3 class="display-4">The more homework a child recieves, the more stress they will be under.</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h3 class="display-3 font-weight-bold">What do we do?</h3>
                    <h3 class="display-4">Provide fun ways of learning and reduce homework stress.</h3>
                </div>
                <div class="col-md-6">
                    <img src="/resources/img/image3.jpg" class="img-fluid">
                </div>
            </div>

   <!--          <div class="row">
                <div class="col-md-12">
                    <div class="display-4 font-weight-bold text-right">Fun Fact</div>
                    <p class="text-monospace">Mathematical anxiety is a real conditon. When certain people encounter math problems (even those who are good at math), they can feel pressured to solve the problems properly, which causes them to make more mistakes, which leads to even more anxiety.</p>
                </div>
            </div> -->

        </div>

    </body>
    <footer class="bg-primary text-white pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Jayasurya Pinaki</p>
                    <p>Student ID : 014491854</p>
                    <p>Contact : jayasurya.pinaki@sjsu.edu</p>
                </div>
                <div class="col-md-6">
                    <p>
                        <a href="https://gitlab.com/jayasurya17" target="_blank" class="text-white">Gitlab</a>
                    </p>
                    <p>
                        <a href="https://www.linkedin.com/in/jayasurya-p/" target="_blank" class="text-white">LinkedIn</a>
                    </p>                 
                </div>
            </div>
        </div>
    </footer>
</html>
