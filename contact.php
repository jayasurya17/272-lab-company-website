<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Elementary</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php 
            require 'navbar.php';
        ?>
        <div class="container mt-5 pt-5 mb-5">

            <?php

                function PrintContactInfo($key, $value) {
                    // <h1> or <h3>
                    echo (strcmp($key, "Name") == 0? '<h1>' : "<h3>");
                    // Key to be displayed bolder than value
                    echo ($key . ': ');
                    // Value to be displayed lighter than key
                    echo ('<span class="font-weight-normal">' . $value . '</span>');
                    // </h1> or </h3>
                    echo (strcmp($key, "Name") == 0? '</h1>' : "</h3>");
                }

                function NoContactsFound() {
                    echo ('<h3 class="display-4 font-weight-bold">Oops! There seems to be a problem. Contacts cannot be displayed at the moment!</h3>');                    
                }

                function DisplayContacts() {

                    $fileLocation = "./resources/contacts.txt"; 
                    // Condition to check if file exists and has some data in it  
                    if(!file_exists($fileLocation) || filesize($fileLocation) == 0) {
                        NoContactsFound();
                    } else {

                        //Opening file
                        $contactsFile = fopen($fileLocation, "r");
                        // Keeps track if there is atleast one valid JSON data in the file
                        $isDataValid = false;
                        while ($data = fgets($contactsFile)) {

                            $data = json_decode($data, true);

                            // If the line does not have a valid JSON data continue to next line
                            if (json_last_error()) {
                                continue;
                            }

                            $isDataValid = true;
                            echo ("<div class='shadow p-5'>");
                            // Condition to check what data the line is holding. Name will be displayed much bigger than the other information
                            foreach ($data as $key => $value) {
                                PrintContactInfo($key, $value);
                            }
                            echo ('</div>');

                        }

                        // Closing file
                        fclose($contactsFile);  

                        //Print error message if the file did not have any valid JSON data
                        if ($isDataValid == false) {
                            NoContactsFound();
                        }     
                    }                 
                }

                function LoginPage($msg) {
                    echo ('<div class="shadow p-5">');
                    echo ('<h3 class="text-center">Access secure section</h3>');
                    // Request will be sent to the same page if no action is provided
                    echo ('<form method="POST">');
                    echo ('<div class="form-group">
                            <label>Username</label>
                            <input type="text" class="form-control" id="username" placeholder="Username" name="username" required>
                          </div>');
                    echo ('<div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
                          </div>');
                    echo ('<p class="text-danger text-center">' . $msg . '</p>');
                    echo ('<button type="submit" class="form-control btn btn-success">Login</button>');
                    echo ("</form>");
                    echo ('</div>');
                }

                if($_SERVER["REQUEST_METHOD"] == "POST") {
                    if ($_POST['username'] === "admin" &&  $_POST['password'] === "admin") {
                        DisplayContacts();
                    } else {
                        LoginPage('Invalid username or password');
                    }
                } else {
                    LoginPage('');
                }

            ?>

        </div>

    </body>
    <footer class="bg-primary text-white pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Jayasurya Pinaki</p>
                    <p>Student ID : 014491854</p>
                    <p>Contact : jayasurya.pinaki@sjsu.edu</p>
                </div>
                <div class="col-md-6">
                    <p>
                        <a href="https://gitlab.com/jayasurya17" target="_blank" class="text-white">Gitlab</a>
                    </p>
                    <p>
                        <a href="https://www.linkedin.com/in/jayasurya-p/" target="_blank" class="text-white">LinkedIn</a>
                    </p>                 
                </div>
            </div>
        </div>
    </footer>
</html>
