<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Elementary</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php 
            require 'navbar.php';
        ?>
        <div class="container mt-5 pt-5">

            <h3><a href="/">Home ></a> <a href="product.php">Products/ Services ></a> Five most visited products</h3>

            <div class="row mt-5">
                <div class="col-md-6"><a href="product.php"><h3>Products/ Services available</h3></a></div>
                <div class="col-md-3"><a href="lastFive.php"><h3>Last 5 visited</h3></a></div>
                <div class="col-md-3"><h3>5 most visited</h3></div>
            </div>


            <?php

                $cookie_name = "mostFiveElementary";
                if(!isset($_COOKIE[$cookie_name])) {
                    echo "<h3 class='mt-5 mb-5'>Oops! Looks like you have not seen any of our products/ services. Please visit <a href='/product.php'>here</a>.";
                } else {
                    $cookie_value = unserialize($_COOKIE[$cookie_name]);
                    arsort($cookie_value);
                    echo '<table class="table mb-5">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Visited count</th>
                                </tr>
                            </thead>
                            <tbody>';

                    $count = 1;
                    foreach ($cookie_value as $key => $value) {
                        $key = explode(",", $key);
                        echo "<tr>";
                            echo  '<th scope="row">' . $count++ . '</th>';
                            echo  '<td><a href="' . $key[1] . '">' . $key[0] . '</a></td>';
                            echo  '<td>' . $value . '</td>';
                        echo "</tr>";
                        if ($count == 6) {
                            break;
                        }
                    }


                    echo "  </tbody>
                        </table>";
                }

            ?>

        </div>

    </body>
    <footer class="bg-primary text-white pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Jayasurya Pinaki</p>
                    <p>Student ID : 014491854</p>
                    <p>Contact : jayasurya.pinaki@sjsu.edu</p>
                </div>
                <div class="col-md-6">
                    <p>
                        <a href="https://gitlab.com/jayasurya17" target="_blank" class="text-white">Gitlab</a>
                    </p>
                    <p>
                        <a href="https://www.linkedin.com/in/jayasurya-p/" target="_blank" class="text-white">LinkedIn</a>
                    </p>                 
                </div>
            </div>
        </div>
    </footer>
</html>
