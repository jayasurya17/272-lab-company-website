<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Elementary</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php 
            require 'navbar.php';
        ?>
        <div class="container mt-5 pt-5">

            <h3 class="display-4 font-weight-bold">Together, we're on the path to success.</h3>

            <h3 class="font-weight-light mt-5" style="text-align: justify;">We are a group of individuals with different tastes, asthetics, opinions and backgrounds. We have all come together to create this wonderful company back in 2010. We work hard to provide a great product which immediately grabs the attention of a child. We invest all our energy in making education fun for kids. We provide custom ways to keep the child engaged while always learning.</h3>

            <h3 class="font-weight-light mt-5 mb-5" style="text-align: justify;">Imagine a classroom extending beyond the school walls where every student can work his or her own pace. Our commitment to quality is inherent to everything we do. Our high quality product has been helping make the lives to children fun and stress free.</h3>

        </div>

    </body>
    <footer class="bg-primary text-white pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Jayasurya Pinaki</p>
                    <p>Student ID : 014491854</p>
                    <p>Contact : jayasurya.pinaki@sjsu.edu</p>
                </div>
                <div class="col-md-6">
                    <p>
                        <a href="https://gitlab.com/jayasurya17" target="_blank" class="text-white">Gitlab</a>
                    </p>
                    <p>
                        <a href="https://www.linkedin.com/in/jayasurya-p/" target="_blank" class="text-white">LinkedIn</a>
                    </p>                 
                </div>
            </div>
        </div>
    </footer>
</html>
