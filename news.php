<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Elementary</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php 
            require 'navbar.php';
        ?>
        <div class="container mt-5">

            <div class="shadow mt-4 p-5">            	
                <h3 class="display-4">Educators Agree on the Value of Ed Tech</h3>
                <p class="font-weight-light" style="text-align: justify;">Americans are increasingly incorporating technology and digital tools into their lives. According to the U.S. Census Bureau, 89% of households in 2016 had a computer (up from 79% in 2015). The swift adoption of new technology has catalyzed the development of new tools to address individuals' and organizations' digital demands, and these demands have decidedly extended into the education space.</p>
                <p class="font-weight-light" style="text-align: justify;">There is growing attention around the effective use of education technology to support students' access to transformative learning opportunities in school. Digital learning tools play an increasingly critical role in innovative programs and practices that support students' access to activities and information that help them prepare for future life and work. Considering the experiences and perspectives of those who select, purchase and use digital learning tools can inform the development and integration of new tools that best support teaching and learning in schools.</p>
                <div class="text-right">
                	<a href="https://www.gallup.com/education/266564/educators-agree-value-tech.aspx" target="_blank">Read more...</a>
                </div>
            </div>

            <div class="shadow mt-4 p-5">            	
                <h3 class="display-4">Digital learning transforms spread of higher education</h3>
                <p class="font-weight-light" style="text-align: justify;">A new phase of promoting higher education which places importance on digital learning through massive open online courses (MOOCs) and online learning platforms has emerged in India, says the latest UNESCO report.</p>
                <p class="font-weight-light" style="text-align: justify;">MOOCs and other digital tools such as Ekalavya, which characterises self-learning environments, have transformed education, connecting students to global learning platforms and making learning more dynamic.</p>
                <p class="font-weight-light" style="text-align: justify;">Authored by Karanam Pushpanadham, under the UNESCO International Institute for Educational Planning, the report Massive Open Online Courses: The emerging landscape of digital learning in India reviews major initiatives undertaken by Indian authorities to facilitate lifelong learning for teachers, students and those in employment.</p>
                <p class="font-weight-light" style="text-align: justify;">India has 462 million internet users with a penetration rate of 35% of the total population. It has the second-largest national group enrolled in MOOCs after the United States, which the Government of India is striving hard to leverage.</p>
                <div class="text-right">
                	<a href="https://www.universityworldnews.com/post.php?story=20190926130129740" target="_blank">Read more...</a>
                </div>
            </div>

            <div class="shadow mt-4 p-5">            	
                <h3 class="display-4">States Have Adopted Dozens of Policies to Support Computer Science Education, Report Finds</h3>
                <p class="font-weight-light" style="text-align: justify;">It's no secret computer science education is seen as one of smartest ways to prepare students for the future of work.</p>
                <p class="font-weight-light" style="text-align: justify;">And in the last 12 months, 33 states have adopted a total of 57 policies to support computer science education, according to a report out today from the nonprofits including Code.org, the Computer Science Teachers Association, and the Expanding Computing Education Pathways Alliance. For instance:</p>
                <ul class="font-weight-light">
                	<li>Alabama set a timeline by which every middle and high school would be required to offer basics in computer science and computational thinking.</li>
                	<li>Connecticut required all teacher preparation programs to include instruction in computer science.</li>
                	<li>Indiana adopted a state computer science plan, made changes to computer science standards and curriculum and is appropriating $3 million to the subject.</li>
                </ul>
                <div class="text-right">
                	<a href="http://blogs.edweek.org/edweek/DigitalEducation/2019/09/computer-science-standards-code-org.html" target="_blank">Read more...</a>
                </div>
            </div>

        </div>

    </body>
    <footer class="bg-primary text-white pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Jayasurya Pinaki</p>
                    <p>Student ID : 014491854</p>
                    <p>Contact : jayasurya.pinaki@sjsu.edu</p>
                </div>
                <div class="col-md-6">
                    <p>
                        <a href="https://gitlab.com/jayasurya17" target="_blank" class="text-white">Gitlab</a>
                    </p>
                    <p>
                        <a href="https://www.linkedin.com/in/jayasurya-p/" target="_blank" class="text-white">LinkedIn</a>
                    </p>                 
                </div>
            </div>
        </div>
    </footer>
</html>
