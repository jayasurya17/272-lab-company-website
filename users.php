<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Elementary</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php 
            require 'navbar.php';
        ?>
        <div class=" mt-5 p-5 pb-5">
            <div class="row">
                <div class="col-md-4">
                    <form action="/addUserToDB.php" method="post">
                        <div class="text-center">
                            <h3>Create new user</h3>
                        </div>
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="firstname" placeholder="Enter your first Name" required="true">
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" name="lastname" placeholder="Enter your last Name" required="true">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Enter your email" required="true">
                        </div>
                        <div class="form-group">
                            <label>Home Address</label>
                            <input type="text" class="form-control" name="address" placeholder="Enter your home address" required="true">
                        </div>
                        <div class="form-group">
                            <label>Home Phone</label>
                            <input type="text" class="form-control" name="home_phone" placeholder="Enter your home phone number" required="true">
                        </div>
                        <div class="form-group">
                            <label>Cell Phone</label>
                            <input type="text" class="form-control" name="cell_phone" placeholder="Enter your cell phone number" required="true">
                        </div>
                        <button type="submit" name="add_user" class="form-control btn btn-primary">Add User</button>
                    </form>
                </div>
                <div class="col-md-8 shadow pt-3">
<!--                     <div class="text-center">
                        <h3>Search for user details by field value</h3>
                    </div> -->
                    <form method="POST">
                        <div class="row">
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="keyword" placeholder="Search by keyword" required="true">           
                            </div>
                            <div class="col-md-4">    
                                <select class="form-control" name="in">
                                    <option value="name">Name</option>
                                    <option value="email">Email</option>
                                    <option value="home_phone">Home Phone</option>
                                    <option value="cell_phone">Cell Phone</option>
                                </select>         
                            </div>
                            <div class="col-md-3">
                                <button class="form-control btn btn-primary" name="search">Search</button>       
                            </div>
                        </div>
                    </form>

                    <?php  
                        function connectToDatabase() {
                            $servername = "localhost";
                            $username = "jayasurya";
                            $password = "272password";
                            $database = "elementary";

                            // Create connection
                            $conn = mysqli_connect($servername, $username, $password, $database);

                            // Check connection
                            if (mysqli_connect_errno()) {
                                die("Connection failed: " . mysqli_connect_error());
                            }
                            // echo "Connected successfully";
                            return $conn;
                        }
                        if($_SERVER["REQUEST_METHOD"] == "POST") {
                            if (isset($_POST['search'])) {
                                // echo "ASdasd";
                                $conn = connectToDatabase();
                                $keyword = $_POST['keyword'];
                                $in = $_POST['in'];
                                if ($in == 'name') {
                                    $SQLquery = "SELECT * from Users where firstname LIKE '%".$keyword."%';";
                                } elseif ($in == "email") {
                                    $SQLquery = "SELECT * from Users where email LIKE '%".$keyword."%';";
                                } elseif ($in == "home_phone") {
                                    $SQLquery = "SELECT * from Users where HomePhone LIKE '%".$keyword."%';";
                                } elseif ($in == "cell_phone") {
                                    $SQLquery = "SELECT * from Users where CellPhone LIKE '%".$keyword."%';";
                                }
                                $searchResult = mysqli_query($conn, $SQLquery);

                                // Check if more than one record is fetched from database 
                                if ($searchResult->num_rows > 0) {    
                                    echo '<div class="table-responsive">
                                            <table class="table mt-5">
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th scope="col">Firstname</th>
                                                        <th scope="col">Lastname</th>
                                                        <th scope="col">Email</th>
                                                        <th scope="col">Home Address</th>
                                                        <th scope="col">Home Phone</th>
                                                        <th scope="col">Cell Phone</th>
                                                    </tr>
                                                </thead>
                                                <tbody>';

                                                $count = 0;
                                                while($row = $searchResult->fetch_assoc()) {
                                                    echo '<tr>';
                                                        echo '<td>'.$row["FirstName"].'</td>';
                                                        echo '<td>'.$row["LastName"].'</td>';
                                                        echo '<td>'.$row["Email"].'</td>';
                                                        echo '<td>'.$row["Address"].'</td>';
                                                        echo '<td>'.$row["HomePhone"].'</td>';
                                                        echo '<td>'.$row["CellPhone"].'</td>';
                                                    echo '</tr>';
                                                }
                                        echo "</tbody>
                                        </table>
                                    </div>";
                                } else {
                                    // Error message to be displayed when no records are fetched from database
                                    echo "<div class='text-center pt-5'>
                                            <h3>Sorry! No results found for the keyword in specified field</h3>
                                        </div>"; 
                                }
                            }
                        } else {
                            // Message to be displayed if the page is loaded with get request
                            echo "<div class='text-center pt-5'>
                                    <h3>Search user database by specifying keyword present in a field</h3>
                                </div>";     
                        }
                    ?>      
                    
                </div>
            </div>

        </div>

    </body>
    <footer class="bg-primary text-white pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Jayasurya Pinaki</p>
                    <p>Student ID : 014491854</p>
                    <p>Contact : jayasurya.pinaki@sjsu.edu</p>
                </div>
                <div class="col-md-6">
                    <p>
                        <a href="https://gitlab.com/jayasurya17" target="_blank" class="text-white">Gitlab</a>
                    </p>
                    <p>
                        <a href="https://www.linkedin.com/in/jayasurya-p/" target="_blank" class="text-white">LinkedIn</a>
                    </p>                 
                </div>
            </div>
        </div>
    </footer>
</html>
