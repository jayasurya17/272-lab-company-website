<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Elementary</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php 
            require 'navbar.php';
        ?>
        <div class="container mt-5 pt-5">

            <h3><a href="/">Home ></a> Products/ Services</h3>

            <div class="row mt-5">
                <div class="col-md-6"><h3>Products/ Services available</h3></div>
                <div class="col-md-3"><a href="lastFive.php"><h3>Last 5 visited</h3></a></div>
                <div class="col-md-3"><a href="mostFive.php"><h3>5 most visited</h3></a></div>
            </div>

            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td><a href="/product/careerCounseling.php">Career counseling</a></td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td><a href="/product/tutorails.php">Tutorials</a></td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td><a href="/product/quizzes.php">Quizzes</a></td>
                </tr>
                <tr>
                    <th scope="row">4</th>
                    <td><a href="/product/discussions.php">Discussions</a></td>
                </tr>
                <tr>
                    <th scope="row">5</th>
                    <td><a href="/product/flashCards.php">Flash cards</a></td>
                </tr>
                <tr>
                    <th scope="row">6</th>
                    <td><a href="/product/games.php">Games</a></td>
                </tr>
                <tr>
                    <th scope="row">7</th>
                    <td><a href="/product/buyTextbooks.php">Buy textbooks</a></td>
                </tr>
                <tr>
                    <th scope="row">8</th>
                    <td><a href="/product/bookReviews.php">Book reviews</a></td>
                </tr>
                <tr>
                    <th scope="row">9</th>
                    <td><a href="/product/universityReviews.php">University reviews</a></td>
                </tr>
                <tr>
                    <th scope="row">10</th>
                    <td><a href="/product/sendTranscripts.php">Send transcripts</a></td>
                </tr>
                </tbody>
            </table>


            <h3 class="font-weight-light mt-5" style="text-align: justify;">Throughout history, textbooks have been a burden to students. They're heavy, expensive and quickly out-of-date. However technology is playing a major role in how education is changing on a whole. <span class="font-weight-bold">94% students</span> say digital education helps them retain new concepts.</h3>

            <div class="mt-5 row text-center">
                <div class="col-sm-12">                    
                    <h3>Digital learning technology can</h3>
                </div>              
                <div class="col-sm-8 offset-sm-2">
                    <div class="row">
                        <div class="col-sm-4 text-center">           
                            <div>
                                <img src="/resources/img/book.png" class="img-fluid">                        
                            </div>
                            <h3 class="font-weight-light">Improve education</h3>
                        </div>               
                        <div class="col-sm-4 text-center">           
                            <div>
                                <img src="/resources/img/clock.png" class="img-fluid">                           
                            </div>
                            <h3 class="font-weight-light">Save time</h3>
                        </div>               
                        <div class="col-sm-4 text-center">           
                            <div>
                                <img src="/resources/img/educate.png" class="img-fluid">   
                            </div>
                            <h3 class="font-weight-light">Boost grades</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-5 row text-center">
                <div class="col-sm-12">                    
                    <h3>Increase your impact</h3>
                </div>              
                <div class="col-sm-8 offset-sm-2">
                    <div class="row">
                        <div class="col-sm-4 text-center">           
                            <div>
                                <img src="/resources/img/about1.jpg" class="img-fluid">                        
                            </div>
                            <h3 class="font-weight-light">Education style that a kid likes</h3>
                        </div>               
                        <div class="col-sm-4 text-center">           
                            <div>
                                <img src="/resources/img/about2.jpg" class="img-fluid">                           
                            </div>
                            <h3 class="font-weight-light">Focus on engagement</h3>
                        </div>               
                        <div class="col-sm-4 text-center">           
                            <div>
                                <img src="/resources/img/about3.jpg" class="img-fluid">   
                            </div>
                            <h3 class="font-weight-light">Invest in your future</h3>
                        </div>
                    </div>
                </div>
            </div>

            <h3 class="display-4 mt-5">Subscription starts from <span class="font-weight-bold">$89.99 per month</span></h3>

            <div class="row mb-5 text-center">
                <div class="col-sm-4 shadow">
                    <h3>Monthly</h3>
                    <h3 class="display-4 font-weight-bold">$109.99</h3>
                </div>
                <div class="col-sm-4 shadow">
                    <h3>Quarterly</h3>
                    <h3 class="display-4 font-weight-bold">$299.99</h3>
                    <p class="text-success">Save 9%</p>
                </div>
                <div class="col-sm-4 shadow">
                    <h3>Annually</h3>
                    <h3 class="display-4 font-weight-bold">$1079.88</h3>
                    <p class="text-success">Save 18%</p>
                </div>
            </div>

        </div>

    </body>
    <footer class="bg-primary text-white pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Jayasurya Pinaki</p>
                    <p>Student ID : 014491854</p>
                    <p>Contact : jayasurya.pinaki@sjsu.edu</p>
                </div>
                <div class="col-md-6">
                    <p>
                        <a href="https://gitlab.com/jayasurya17" target="_blank" class="text-white">Gitlab</a>
                    </p>
                    <p>
                        <a href="https://www.linkedin.com/in/jayasurya-p/" target="_blank" class="text-white">LinkedIn</a>
                    </p>                 
                </div>
            </div>
        </div>
    </footer>
</html>
