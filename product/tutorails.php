<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Elementary</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php 

            $cookie_name = "lastFiveElementary";
            $pageKey = "Tutorials";
            $pageURL = "/product/tutorails.php";
            $pageValue = array($pageKey, $pageURL);
            if(!isset($_COOKIE[$cookie_name])) {
                $cookie_value = array();
                array_push($cookie_value, $pageValue);
            } else {
                $cookie_value = unserialize($_COOKIE[$cookie_name]);

                if (in_array($pageValue, $cookie_value)) {
                    unset($cookie_value[array_search($pageValue, $cookie_value)]);
                }
                
                if (count($cookie_value) == 5) {
                    array_pop($cookie_value);
                }
                
                // print_r(unserialize($_COOKIE[$cookie_name]));
                array_unshift($cookie_value, $pageValue);
            }
            setcookie($cookie_name, serialize($cookie_value), time() + (86400 * 30), "/");


            $cookie_name = "mostFiveElementary";
            $pageKey = "Tutorials";
            $pageURL = "/product/tutorails.php";
            $cookieKey = $pageKey . "," . $pageURL;

            if (!isset($_COOKIE[$cookie_name])) {
                $cookie_value = array(
                    $cookieKey => 1
                );
            } else {
                $cookie_value = unserialize($_COOKIE[$cookie_name]);
                if(array_key_exists($cookieKey, $cookie_value)) {
                    $cookie_value[$cookieKey] = $cookie_value[$cookieKey] + 1;
                } else {
                    $cookie_value[$cookieKey] = 1;
                }
                // print_r($cookie_value);
            }
            setcookie($cookie_name, serialize($cookie_value), time() + (86400 * 30), "/");
            
            require 'navbar.php';
        ?>
        <div class="container mt-5 pt-5">

            <h3><a href="../">Home ></a> <a href="../product.php">Products/ Services ></a> Tutorials</h3>

            <div class="mt-5 mb-5 text-center">
                <img src="../../resources/product/tutorails.jpg" class="img-fluid">
            </div>

            <h3 class="font-weight-light mb-5 text-justify">Tutorial helps learners enhance their intellectual, communication and social skills. Even those who attend tutorials do so without adequate preparation although they are given one week to study the topic.</h3>

            
            
        </div>

    </body>
    <footer class="bg-primary text-white pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Jayasurya Pinaki</p>
                    <p>Student ID : 014491854</p>
                    <p>Contact : jayasurya.pinaki@sjsu.edu</p>
                </div>
                <div class="col-md-6">
                    <p>
                        <a href="https://gitlab.com/jayasurya17" target="_blank" class="text-white">Gitlab</a>
                    </p>
                    <p>
                        <a href="https://www.linkedin.com/in/jayasurya-p/" target="_blank" class="text-white">LinkedIn</a>
                    </p>                 
                </div>
            </div>
        </div>
    </footer>
</html>
