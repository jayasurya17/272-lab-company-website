
        <nav class="navbar navbar-expand-lg navbar-dark bg-info fixed-top">
            <a class="navbar-brand" href="/">Elementary</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="../">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../about.php">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../product.php">Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../lastFive.php">Last five</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../mostFive.php">Most five</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../news.php">News</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../contact.php">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../users.php">Users</a>
                    </li>
                </ul>
            </div>
        </nav>